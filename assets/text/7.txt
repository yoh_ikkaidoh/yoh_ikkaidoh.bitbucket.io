お前の目付役アリーがドアを叩いた音で目を覚ました。覚えているね。嫌な予感がした。
「こんな早朝になんだよ」
「お兄様が死にました。ダマスカスです」
　冗談だと思っていただろう。政府の人間にありがちな、きつい冗談だと。軍人のジョークセンスは文民にはわかりにくかった。ご子息様、今日はビールフェスのようですね。信者がどのくらい死ぬか賭けませんか？　アリーは、この種類のからかいをよくした。お前はあまりいい気分じゃなかったね。
「アリー、冗談はやめろ。朝の３時だぞ？　故郷の時計に合わせていても、早起きすぎだ、今日が別に――」
　そしてお前は、ハサンの目が完全にまともで、そして、うろたえていることに気がついた。街灯が徐々に消えていき、それと同時に、青白い朝が訪れようとしていた。
