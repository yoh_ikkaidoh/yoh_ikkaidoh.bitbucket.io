彼がまた引き金を引いた。血だまりがお前の足下まで迫って、お前はとっさに足を引いてしまった。
「何で私が銃を撃つか分かるか？」
　間。
　父親がさらに死体に銃弾を撃ち込んだ。その従者はラーシドという男だった。車が好きだと言っていた。銃弾が撃ち込まれた。父親が再び尋ねた。
「お前は私が何で銃を撃つか分からないのか？」
　間。
　銃声が二発響いた。
「お前は私が銃を撃つ理由が分からないのか？」
「分かりません」
　お前は震える声で絞り出した。父親が「分からないのか？」と聞き返した。血の臭いがお前にまとわりつき始めた。手が震えた。床が妙に近く感じた。流れ出した血がぐるぐるとらせんを描きながらお前と父親を取り囲んで、外に向かって伸びていった。
「分かりません！」
「もう一度言うんだ」
「私には理由が分かりません！」
　間。
